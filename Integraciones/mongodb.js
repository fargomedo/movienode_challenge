const mongoose = require('mongoose');
mongoose.promise = global.promise

const db = mongoose.connection

mongoose.connect('mongodb://mongodb/mongodatabase', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(db)
    .catch(err => console.log(err));

module.exports = db;
