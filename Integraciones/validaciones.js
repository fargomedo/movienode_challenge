'use strict'
const Joi = require('joi');
let year = new Date().getFullYear();

const validaYear = Joi.object({
    year: Joi.number().integer().max(parseInt(year))
});

const validaTitulo = Joi.object({
    titulo: Joi.string().min(1)
});

const validaPagina = Joi.object({
    pagina: Joi.number().integer().min(1)
});

const validaPostBody = Joi.object({
    pelicula: Joi.string().min(1).required(),
    buscar: Joi.string().min(1).max(255).required(),
    reemplazar: Joi.string().min(1).max(255).required(),
});

module.exports = {
    validaYear,
    validaTitulo,
    validaPagina,
    validaPostBody,
}

