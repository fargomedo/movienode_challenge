'use strict'
const mongoose = require('mongoose');
const paginacion = require('mongoose-paginate-v2');
const esquema = new mongoose.Schema({
    id: {
        type: Number
    },
    Title: {
        type: String,
        trim: true
    },
    Year: {
        type: Number
    },
    Released: {
        type: Date
    },
    Genre: {
        type: String,
        trim: true
    },
    Director: {
        type: String,
        trim: true
    },
    Actors: {
        type: String,
        trim: true
    },
    Plot: {
        type: String,
        trim: true
    },
    Ratings: Object
});

esquema.plugin(paginacion);

module.exports = mongoose.model('pelicula', esquema, 'peliculas');