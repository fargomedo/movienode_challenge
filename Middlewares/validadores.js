'use strict'

const validaciones = require('../Integraciones/validaciones');

async function validaGetPelicula(ctx, next) {
    let validaHeader = validaciones.validaYear.validate(ctx.request.headers, { allowUnknown: true });
    let validaTitulo = validaciones.validaTitulo.validate(ctx.request.body);

    if (validaHeader.error) {
        ctx.status = 400;
        ctx.body = validaHeader.error.details[0].message;
    } else if (validaTitulo.error) {
        ctx.status = 400;
        ctx.body = validaTitulo.error.details[0].message;
    } else {
        await next();
    }
}

async function validaGetPaginacion(ctx, next) {
    let validaPagina = validaciones.validaPagina.validate(ctx.request.headers, { allowUnknown: true });

    if (validaPagina.error) {
        ctx.status = 400;
        ctx.body = validaPagina.error.details[0].message;
    } else {
        await next();
    }
}

async function validaEditPlot(ctx, next) {
    let validaBody = validaciones.validaPostBody.validate(ctx.request.body);

    if (validaBody.error) {
        ctx.status = 400;
        ctx.body = validaBody.error.details[0].message;
    } else {
        await next();
    }
}


module.exports = {
    validaGetPelicula,
    validaGetPaginacion,
    validaEditPlot
}
