'use strict'
const Koa = require('koa');
const KoaRouter = require('koa-router');
const KoaRespond = require('koa-respond');
const KoaCors = require('@koa/cors');
const KoaBodyparser = require('koa-bodyparser');


require('./Integraciones/mongodb.js');

const app = new Koa();
const router = new KoaRouter();
const rutaPeliculas = require('./Rutas/pelicula.router');

app
    .use(KoaRespond())
    .use(KoaCors())
    .use(KoaBodyparser())
    .use(rutaPeliculas.routes())
    .use(router.allowedMethods());

app.listen(3000);

module.exports = app;