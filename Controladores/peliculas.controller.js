'use strict'
var peliculaModel = require('../Modelos/pelicula.model');
const axios = require('axios');

async function buscar(ctx) {
    try {
        let query = '';
        let validado = false;
        let baseUrl = 'https://www.omdbapi.com/';
        let key = '&apikey=72d0abf0';
        let movieYear = ctx.request.header.year || false;
        let movieTitle = ctx.request.query.title ? ctx.request.query.title.toLowerCase().trim() : false;

        if (movieTitle || movieYear) {
            if (movieTitle) {
                query = `?t=${movieTitle}`;
            }
            if (movieYear) {
                query = (query != '') ? `${query}&y=${movieYear}` : `?y=${movieYear}`;
            }
            validado = true
        }

        if (validado) {
            query = baseUrl + query + key;
            let solicitud = await axios.get(query);
            let data = solicitud.data;
            if (data.Response == 'True') {
                let pelicula = await peliculaModel.find({ Title: data.Title }).exec();
                if (pelicula.length == 0) {
                    const nuevaPelicula = new peliculaModel({
                        ID: data.imdbID,
                        Title: data.Title,
                        Year: data.Year,
                        Released: data.Released,
                        Genre: data.Genre,
                        Director: data.Director,
                        Actors: data.Actors,
                        Plot: data.Plot,
                        Ratings: data.Ratings
                    });
                    ctx.body = await nuevaPelicula.save();
                } else {
                    ctx.status = 200;
                    ctx.body = pelicula;
                }
            } else {
                ctx.status = 200;
                ctx.body = 'No existen resultados para su b�squeda';
            }
        } else {
            ctx.status = 400;
            ctx.body = 'Debe proporcionar al menos un campo de b�squeda.';
        }
    } catch (error) {
        ctx.body = { message: error };
        ctx.status = 404;
    }
}

async function listar(ctx) {
    try {
        let cuenta = await peliculaModel.find().estimatedDocumentCount();
        let pagina = ctx.request.headers.pagina;
        if (cuenta > 5) {
            ctx.body = await peliculaModel.paginate({}, {
                page: pagina ? pagina : 1,
                limit: 5
            });
        } else {
            ctx.body = await peliculaModel.find().exec();
        }

    } catch (error) {
        ctx.body = { message: error };
        ctx.status = 404;
    }

}

async function editPlot(ctx) {
    try {
        let parametros = ctx.request.body;
        let pelicula = await peliculaModel.findOne({ Title: parametros.pelicula }).exec();
        if (pelicula) {
            let plot = pelicula.Plot;
            ctx.status = 200;
            ctx.body = plot.replace(new RegExp(parametros.buscar, 'g'), parametros.reemplazar);
        } else {
            ctx.status = 200;
            ctx.body = 'No existen resultados para su b�squeda';
        }
    } catch (err) {
        ctx.body = err;
        ctx.status = 404;
    }

}

module.exports = {
    buscar,
    listar,
    editPlot
}
