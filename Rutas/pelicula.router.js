'use strict'
const peliculasController = require('../Controladores/peliculas.controller');
const middlewares = require('../Middlewares/validadores');
const KoaRouter = require('koa-router');
const rutas = new KoaRouter();

rutas.get('/pelicula', middlewares.validaGetPelicula, peliculasController.buscar);
rutas.get('/todas', middlewares.validaGetPaginacion, peliculasController.listar);
rutas.post('/edita', middlewares.validaEditPlot, peliculasController.editPlot);

module.exports = rutas;
