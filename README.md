# MovieNode Challenge! 🍿

Versión del reto que solicitaba construir a través de NodeJS un microservicio con 2 endpoints que permita buscar, guardar, recuperar y editar peliculas.

## Instalación 🧑‍💻

Utilizar los siguientes comandos para generar y levantar el contenedor de docker que contiene los servicios y la BD de Mongo:

```bash
docker-compose build
docker-compose up
```

## Uso 🗺️

Las peticiones se pueden realizar mediante postman o la web mediante la url localhost:4000 a las siguientes rutas

```bash
GET- localhost:4000/peliculas
GET- localhost:4000/todas
POST- localhost:4000/editPlot
```

## Documentación 📚

La documentación de la API se encuentra aojada en swaggerhub, desde donde se podrán realizar peticiones a la API si es que el contenedor se encuentra arriba.

[swaggerhub](https://app.swaggerhub.com/apis/fargomedoa/ChallengeCLM/1.0.0#/)


## License
[MIT](https://choosealicense.com/licenses/mit/)
